use rltk::{console, Point};
use specs::prelude::*;

use crate::{pos_point, xy_idx, Map, Monster, Name, Position, Viewshed};

pub struct MonsterAI {}

impl<'a> System<'a> for MonsterAI {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadExpect<'a, Map>,
        ReadExpect<'a, Point>,
        WriteStorage<'a, Viewshed>,
        ReadStorage<'a, Monster>,
        ReadStorage<'a, Name>,
        WriteStorage<'a, Position>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (map, player_pos, mut viewshed, monster, name, mut position) = data;

        for (mut viewshed, _monster, name, mut pos) in
            (&mut viewshed, &monster, &name, &mut position).join()
        {
            if viewshed.visible_tiles.contains(&*player_pos) {
                let distance =
                    rltk::DistanceAlg::Pythagoras.distance2d(pos_point(pos), *player_pos);
                if distance < 1.5 {
                    // Attack goes here
                    console::log(&format!("{} shouts insults", name.name));
                    return;
                }

                let path = rltk::a_star_search(
                    xy_idx(&*map, pos.x, pos.y) as i32,
                    xy_idx(&*map, player_pos.x, player_pos.y) as i32,
                    &*map,
                );

                if path.success && path.steps.len() > 1 {
                    pos.x = path.steps[1] as i32 % map.width;
                    pos.y = path.steps[1] as i32 / map.width;
                    viewshed.dirty = true;
                }
            }
        }
    }
}
